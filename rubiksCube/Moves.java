package rubiksCube;

public enum Moves {
	U(CubeFaces.U, 1), U2(CubeFaces.U, 2), U3(CubeFaces.U, 3),
	D(CubeFaces.D, 1), D2(CubeFaces.D, 2), D3(CubeFaces.D, 3),
	F(CubeFaces.F, 1), F2(CubeFaces.F, 2), F3(CubeFaces.F, 3),
	B(CubeFaces.B, 1), B2(CubeFaces.B, 2), B3(CubeFaces.B, 3),
	L(CubeFaces.L, 1), L2(CubeFaces.L, 2), L3(CubeFaces.L, 3),
	R(CubeFaces.R, 1), R2(CubeFaces.R, 2), R3(CubeFaces.R, 3);
	
	private CubeFaces face;
	private int numTurns;
	
	Moves(CubeFaces face, int numTurns) {
		this.face = face;
		this.numTurns = numTurns;
	}
	
	public CubeFaces getFace() {
		return face;
	}
	
	public int getNumTurns() {
		return numTurns;
	}
}
