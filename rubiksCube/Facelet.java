package rubiksCube;

class Facelet {
	
	// which face this facelet belongs to
	CubeFaces face;
	// which face this facelets is currently on
	CubeFaces currentFace;
	
	public Facelet(CubeFaces face, CubeFaces currentFace) {
		this.face = face;
		this.currentFace = currentFace;
	}
	
	public Facelet(Facelet f) {
		this.face = f.getFace();
		this.currentFace = f.getCurrentFace();
	}
	
	public CubeFaces getFace() {
		return face;
	}
	
	public CubeFaces getCurrentFace() {
		return currentFace;
	}
	
	public void setCurrentFace(CubeFaces value) {
		this.currentFace = value;
	}
}
