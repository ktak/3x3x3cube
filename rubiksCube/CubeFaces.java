package rubiksCube;

public enum CubeFaces {
	// up, down, front, back, left, right
	U, D, F, B, L, R;
	
	// returns the face clockwise to this face when rotatedFace
	//  is considered the front face
	public CubeFaces getClockwiseFace(CubeFaces rotatedFace) {
		switch (rotatedFace) {
		case B:
			switch (this) {
			case D:
				return R;
			case L:
				return D;
			case R:
				return U;
			case U:
				return L;
			default:
				return this;
			}
		case D:
			switch (this) {
			case B:
				return L;
			case F:
				return R;
			case L:
				return F;
			case R:
				return B;
			default:
				return this;
			}
		case F:
			switch (this) {
			case D:
				return L;
			case L:
				return U;
			case R:
				return D;
			case U:
				return R;
			default:
				return this;
			}
		case L:
			switch (this) {
			case B:
				return U;
			case D:
				return B;
			case F:
				return D;
			case U:
				return F;
			default:
				return this;
			}
		case R:
			switch (this) {
			case B:
				return D;
			case D:
				return F;
			case F:
				return U;
			case U:
				return B;
			default:
				return this;
			}
		// case U
		default:
			switch (this) {
			case B:
				return R;
			case F:
				return L;
			case L:
				return B;
			case R:
				return F;
			default:
				return this;
			}
		}
	}
}

