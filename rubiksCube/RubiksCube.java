package rubiksCube;

import java.util.EnumMap;

public class RubiksCube {
	
	private EnumMap<CubiePositions, Cubie> positions =
			new EnumMap<CubiePositions, Cubie>(CubiePositions.class);
	
	private Cubie createEdgeCubie(CubeFaces face1, CubeFaces currentFace1,
			CubeFaces face2, CubeFaces currentFace2) {
		Facelet[] f = new Facelet[2];
		f[0] = new Facelet(face1, currentFace1);
		f[1] = new Facelet(face2, currentFace2);
		return new Cubie(f);
	}
	
	private Cubie createCornerCubie(CubeFaces face1, CubeFaces currentFace1,
			CubeFaces face2, CubeFaces currentFace2,
			CubeFaces face3, CubeFaces currentFace3) {
		Facelet[] f = new Facelet[3];
		f[0] = new Facelet(face1, currentFace1);
		f[1] = new Facelet(face2, currentFace2);
		f[2] = new Facelet(face3, currentFace3);
		return new Cubie(f);
	}
	
	// construct solved cube
	public RubiksCube() {
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.CORNER) {
				CubeFaces[] face = pos.getFaces();
				Cubie newCubie = createCornerCubie(
						face[0], face[0],
						face[1], face[1],
						face[2], face[2]);
				positions.put(pos, newCubie);
			}
			// must be edge cubie
			else {
				CubeFaces[] face = pos.getFaces();
				Cubie newCubie = createEdgeCubie(
						face[0], face[0],
						face[1], face[1]);
				positions.put(pos, newCubie);
			}
		}
	}
	
	public void rotateFaceClockwise(CubeFaces face) {
		CubiePositions[][] perm = CubiePositions.getPerm(face);
		for (CubiePositions[] cycle : perm) {
			Cubie newVal = positions.get(cycle[cycle.length-1]);
			for (CubiePositions pos : cycle) {
				newVal.rotateClockwise(face);
				newVal = positions.put(pos, newVal);
			}
		}
	}
	
	public void printState() {
		for (CubiePositions pos : CubiePositions.values()) {
			Cubie c = positions.get(pos);
			c.print();
			System.out.println(" in position " + pos);
		}
	}
}
