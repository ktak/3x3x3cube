package rubiksCube;

public class Cubie {
	
	private Facelet[] facelets;
	
	Cubie(Facelet[] facelets) {
		this.facelets = new Facelet[facelets.length];
		for (int i=0; i < facelets.length; i++) {
			this.facelets[i] = new Facelet(facelets[i]);
		}
	}
	
	// rotates the facelets on this cubie to their clockwise face
	public void rotateClockwise(CubeFaces faceRotated) {
		for (Facelet facelet : facelets) {
			CubeFaces newFace =
				facelet.getCurrentFace().getClockwiseFace(faceRotated);
			facelet.setCurrentFace(newFace);
		}
	}
	
	public void print() {
		for (Facelet f : facelets) {
			System.out.print(f.getFace() + " on " + f.getCurrentFace() + " ");
		}
	}
}
