package rubiksCube;

public enum CubiePositions {
	// Corners
	UFR(CubieTypes.CORNER, new CubeFaces[]{CubeFaces.U, CubeFaces.F, CubeFaces.R}),
	UBL(CubieTypes.CORNER, new CubeFaces[]{CubeFaces.U, CubeFaces.B, CubeFaces.L}),
	DFL(CubieTypes.CORNER, new CubeFaces[]{CubeFaces.D, CubeFaces.F, CubeFaces.L}),
	DBR(CubieTypes.CORNER, new CubeFaces[]{CubeFaces.D, CubeFaces.B, CubeFaces.R}),
	DBL(CubieTypes.CORNER, new CubeFaces[]{CubeFaces.D, CubeFaces.B, CubeFaces.L}),
	DFR(CubieTypes.CORNER, new CubeFaces[]{CubeFaces.D, CubeFaces.F, CubeFaces.R}),
	UBR(CubieTypes.CORNER, new CubeFaces[]{CubeFaces.U, CubeFaces.B, CubeFaces.R}),
	UFL(CubieTypes.CORNER, new CubeFaces[]{CubeFaces.U, CubeFaces.F, CubeFaces.L}),
	// Edges
	UF(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.U, CubeFaces.F}),
	DF(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.D, CubeFaces.F}),
	FL(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.F, CubeFaces.L}),
	FR(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.F, CubeFaces.R}),
	UB(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.U, CubeFaces.B}),
	DB(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.D, CubeFaces.B}),
	BL(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.B, CubeFaces.L}),
	BR(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.B, CubeFaces.R}),
	UL(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.U, CubeFaces.L}),
	DL(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.D, CubeFaces.L}),
	UR(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.U, CubeFaces.R}),
	DR(CubieTypes.EDGE, new CubeFaces[]{CubeFaces.D, CubeFaces.R});
	
	private CubieTypes type;
	private CubeFaces[] faces;
	
/* permutations for face rotations */
// these define the cycles of position substitutions
// when the corresponding face is rotated clockwise
	
	private static final CubiePositions[][] upPerm = {
		{CubiePositions.UBL, CubiePositions.UBR, CubiePositions.UFR, CubiePositions.UFL},
		{CubiePositions.UL, CubiePositions.UB, CubiePositions.UR, CubiePositions.UF}};
	
	private static final CubiePositions[][] downPerm = {
		{CubiePositions.DFL, CubiePositions.DFR, CubiePositions.DBR, CubiePositions.DBL},
		{CubiePositions.DL, CubiePositions.DF, CubiePositions.DR, CubiePositions.DB}};
	
	private static final CubiePositions[][] frontPerm = {
		{CubiePositions.UFL, CubiePositions.UFR, CubiePositions.DFR, CubiePositions.DFL},
		{CubiePositions.FL, CubiePositions.UF, CubiePositions.FR, CubiePositions.DF}};
	
	private static final CubiePositions[][] backPerm = {
		{CubiePositions.UBR, CubiePositions.UBL, CubiePositions.DBL, CubiePositions.DBR},
		{CubiePositions.BR, CubiePositions.UB, CubiePositions.BL, CubiePositions.DB}};
	
	private static final CubiePositions[][] leftPerm = {
		{CubiePositions.UBL, CubiePositions.UFL, CubiePositions.DFL, CubiePositions.DBL},
		{CubiePositions.BL, CubiePositions.UL, CubiePositions.FL, CubiePositions.DL}};
	
	private static final CubiePositions[][] rightPerm = {
		{CubiePositions.UFR, CubiePositions.UBR, CubiePositions.DBR, CubiePositions.DFR},
		{CubiePositions.FR, CubiePositions.UR, CubiePositions.BR, CubiePositions.DR}};
	
/* end permutations */
	
	private CubiePositions(CubieTypes type, CubeFaces[] faces) {
		this.type = type;
		this.faces = faces;
	}
	
	public static CubiePositions[][] getPerm(CubeFaces face) {
		switch (face) {
		case B:
			return backPerm;
		case D:
			return downPerm;
		case F:
			return frontPerm;
		case L:
			return leftPerm;
		case R:
			return rightPerm;
		// case U
		default:
			return upPerm;
		}
	}
	
	public CubieTypes getType() {
		return type;
	}
	
	public CubeFaces[] getFaces() {
		return faces;
	}
	
	public static int getNumCorners() {
		return 8;
	}
	
	public static int getNumEdges() {
		return 12;
	}
	
	public enum CubieTypes {
		CORNER, EDGE;
	}
}

