package thistlethwaite;

import java.util.EnumMap;

import rubiksCube.CubiePositions;
import rubiksCube.Moves;

// phase 1 orients all of the edge cubies such that
//  none of them requires an odd number of up or down
//  face turns to place them in their solved positions
class Phase1 implements Phase {
	
	public Phase1() {
		PhaseTableGenerator.fillTable(this);
	}
	
	private static final Moves[] allowedMoves = {
		Moves.U, Moves.U2, Moves.U3,
		Moves.D, Moves.D2, Moves.D3,
		Moves.F, Moves.F2, Moves.F3,
		Moves.B, Moves.B2, Moves.B3,
		Moves.L, Moves.L2, Moves.L3,
		Moves.R, Moves.R2, Moves.R3
	};

	private static final int TABLE_SIZE = 1<<CubiePositions.getNumEdges();
	private static final byte[] table = new byte[TABLE_SIZE];
	// uninitialized value for table
	static final int goalCoord =
			coord(CoordinateHelper.getSolvedOrientations(),
			CoordinateHelper.getSolvedPositions());
	
	private static void setcoord(int coord,
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		CoordinateHelper.setEdgeOriCoord(coord, orientations);
	}
	
	private static int coord(EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		return CoordinateHelper.getEdgeOriCoord(orientations);
	}

	@Override
	public byte[] getPruningTable() {
		return table;
	}

	@Override
	public Moves[] getAllowedMoves() {
		return allowedMoves;
	}

	@Override
	public int getCoord(EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		return CoordinateHelper.getEdgeOriCoord(orientations);
	}

	@Override
	public int getGoalCoord() {
		return goalCoord;
	}

	@Override
	public int getPhaseNum() {
		return 1;
	}

	@Override
	public void setCoord(int coord,
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		setcoord(coord, orientations, positions);
	}
}
