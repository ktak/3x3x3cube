package thistlethwaite;

import java.util.EnumMap;
import java.util.LinkedList;

import rubiksCube.CubiePositions;
import rubiksCube.Moves;

public class Phase4 implements Phase {

	private static final Moves[] allowedMoves = {
		Moves.U2,
		Moves.D2,
		Moves.F2,
		Moves.B2,
		Moves.L2,
		Moves.R2
	};
	
	private static final byte[] table = createPhase4PruningTable();
	// uninitialized value for table
	static final byte UNDEF = -1;
	static final int goalCoord = coord(CoordinateHelper.getSolvedOrientations(), CoordinateHelper.getSolvedPositions());
	
	private static byte[] createPhase4PruningTable() {
		EnumMap<CubiePositions, Integer> orientations =
				CoordinateHelper.getSolvedOrientations();
		EnumMap<CubiePositions, CubiePositions> positions =
				CoordinateHelper.getSolvedPositions();
		byte[] table = new byte[1<<21];
		for (int i=0; i < 1<<21; i++) {
			table[i] = UNDEF;
		}
		int fromCoord = coord(orientations, positions);
		byte depth = 0;
		int numAtDepth = 0;
		// the goal state is distance zero from the goal state
		table[fromCoord] = depth;
		LinkedList<Integer> queue = new LinkedList<Integer>();
		queue.add(fromCoord);
		while (!queue.isEmpty()) {
			fromCoord = queue.remove();
			setcoord(fromCoord, orientations, positions);
			if (fromCoord != coord(orientations, positions)) {
				System.out.println("fromCoord: " + fromCoord);
				System.out.println("actual: " + coord(orientations, positions));
				throw new IllegalArgumentException();
			}
			if (table[fromCoord] != depth) {
				System.out.println(numAtDepth + " at depth " + (depth+1));
				numAtDepth = 0;
				depth = table[fromCoord];
			}
			//System.out.println("coord: " + fromCoord);
			for (Moves move : allowedMoves) {
				for (int i=0; i < move.getNumTurns(); i++) {
					CoordinateHelper.
						turnClockwise(move.getFace(), orientations, positions);
				}
				int toCoord = coord(orientations, positions);
				if (table[toCoord] == UNDEF) {
					table[toCoord] = (byte)(depth+1);
					queue.add(toCoord);
					numAtDepth++;
				}
				// restore the orientations by rotating a total of 4 times
				for (int i=move.getNumTurns(); i < 4; i++) {
					CoordinateHelper.
						turnClockwise(move.getFace(), orientations, positions);
				}
			}
		}
		
		return table;
	}
	
	private static void setcoord(int coord,
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		CoordinateHelper.setTetradPermCoord(coord / (24*24*24), positions);
		CoordinateHelper.setLRSlicePermCoord((coord / (24*24)) % 24, positions);
		CoordinateHelper.setUDSlicePermCoord((coord / 24) % 24, positions);
		CoordinateHelper.setFBSlicePermCoord(coord % 24, positions);
	}
	
	private static int coord(EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		return (CoordinateHelper.getTetradPermCoord(positions) * 24 * 24 * 24) +
				(CoordinateHelper.getLRSlicePermCoord(positions) * 24 * 24) +
				(CoordinateHelper.getUDSlicePermCoord(positions) * 24) +
				(CoordinateHelper.getFBSlicePermCoord(positions));
	}
	
	@Override
	public byte[] getPruningTable() {
		return table;
	}

	@Override
	public Moves[] getAllowedMoves() {
		return allowedMoves;
	}

	@Override
	public int getCoord(EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		return coord(orientations, positions);
	}

	@Override
	public int getGoalCoord() {
		return goalCoord;
	}

	@Override
	public int getPhaseNum() {
		return 3;
	}

	@Override
	public void setCoord(int coord,
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		// TODO Auto-generated method stub
		
	}
}
