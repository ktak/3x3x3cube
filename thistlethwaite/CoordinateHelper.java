package thistlethwaite;

import java.util.EnumMap;

import rubiksCube.CubeFaces;
import rubiksCube.CubiePositions;


public class CoordinateHelper {
	
	// TODO create arrays for corners, edges, tetrad 1, tetrad 2, slices, etc. so
	// as not to have to loop through every position
	
	static final int ORIENTATION_GOOD = 0;
	// edge orientation flipped
	static final int ORIENTATION_FLIPPED = 1;
	// corner orientation twisted clockwise
	static final int ORIENTATION_C = 1;
	// corner orientation twisted counter-clockwise
	static final int ORIENTATION_CC = 2;
	
/* orientation changes for clockwise face rotations */
// these are dependent upon the definitions of a flipped or rotated cubie,
//  so are specific to this algorithm and the subgroups used to solve the
//  cube. the reference faces for edges are the front or back face, or
//  the up or down face for the edges that have neither a front or back
//  face. the reference for corners is the left or right face
// ORIENTATION_GOOD means no change
	static int getOriChange(CubeFaces face, CubiePositions pos) {
		switch (face) {
		case B:
			switch (pos) {
			case UBR:
			case DBL:
				return ORIENTATION_C;
			case UBL:
			case DBR:
				return ORIENTATION_CC;
			case BR:
			case UB:
			case BL:
			case DB:
				return ORIENTATION_GOOD;
			default:
				throw new IllegalArgumentException();
			}
		case D:
			switch (pos) {
			case DFL:
			case DBR:
				return ORIENTATION_C;
			case DFR:
			case DBL:
				return ORIENTATION_CC;
			case DL:
			case DF:
			case DR:
			case DB:
				return ORIENTATION_FLIPPED;
			default:
				throw new IllegalArgumentException();
			}
		case F:
			switch (pos) {
			case UFL:
			case DFR:
				return ORIENTATION_C;
			case UFR:
			case DFL:
				return ORIENTATION_CC;
			case FL:
			case UF:
			case FR:
			case DF:
				return ORIENTATION_GOOD;
			default:
				throw new IllegalArgumentException();
			}
		case L:
			switch (pos) {
			case UBL:
			case DFL:
			case UFL:
			case DBL:
				return ORIENTATION_GOOD;
			case BL:
			case UL:
			case FL:
			case DL:
				return ORIENTATION_GOOD;
			default:
				throw new IllegalArgumentException();
			}
		case R:
			switch (pos) {
			case UFR:
			case DBR:
			case UBR:
			case DFR:
				return ORIENTATION_GOOD;
			case FR:
			case UR:
			case BR:
			case DR:
				return ORIENTATION_GOOD;
			default:
				throw new IllegalArgumentException();
			}
		// case U
		default:
			switch (pos) {
			case UBL:
			case UFR:
				return ORIENTATION_C;
			case UBR:
			case UFL:
				return ORIENTATION_CC;
			case UL:
			case UB:
			case UR:
			case UF:
				return ORIENTATION_FLIPPED;
			default:
				throw new IllegalArgumentException();
			}
		}
	}
	
	static int getTetradPermCoord(EnumMap<CubiePositions, CubiePositions> positions) {
		int coord = 0;
		final CubiePositions[] tetrad1 = { CubiePositions.UFR, CubiePositions.UBL, CubiePositions.DFL, CubiePositions.DBR };
		final CubiePositions[] tetrad2 = { CubiePositions.DBL, CubiePositions.DFR, CubiePositions.UBR, CubiePositions.UFL };
		final int[] multipliers = { 1, 1, 2, 6 }; // 0!, 1!, 2!, 3!
		int numCubiesHigher = 0;
		// Get permutation of tetrad 1
		for (int i=0; i < tetrad1.length; i++) {
			CubiePositions cubieInPos = positions.get(tetrad1[i]);
			numCubiesHigher = 0;
			// Count the number of cubies at lower positions with a higher ordinality
			for (int j=0; j < i; j++) {
				int ordinal1 = 0, ordinal2 = 0;
				switch (positions.get(tetrad1[j])) {
				case UFR:
					ordinal1 = 0;
					break;
				case UBL:
					ordinal1 = 1;
					break;
				case DFL:
					ordinal1 = 2;
					break;
				case DBR:
					ordinal1 = 3;
					break;
				default:
					break;
				}
				switch (cubieInPos) {
				case UFR:
					ordinal2 = 0;
					break;
				case UBL:
					ordinal2 = 1;
					break;
				case DFL:
					ordinal2 = 2;
					break;
				case DBR:
					ordinal2 = 3;
					break;
				default:
					break;
				}
				if (ordinal1 > ordinal2) {
					numCubiesHigher++;
				}
			}
			coord += multipliers[i] * numCubiesHigher;
		}
		// For a given permutation of tetrad 1, the permutation of
		//  tetrad 2 is known by only one position
		CubiePositions tetrad2FirstCubie = positions.get(tetrad2[0]);
		int i;
		for (i=0; i < tetrad2.length; i++) {
			if (tetrad2FirstCubie == tetrad2[i]) {
				break;
			}
		}
		coord <<= 2;
		coord += i;
		return coord;
	}
	
	static void setTetradPermCoord(int coord, EnumMap<CubiePositions, CubiePositions> positions) {
		final CubiePositions[] tetrad1 = { CubiePositions.UFR, CubiePositions.UBL, CubiePositions.DFL, CubiePositions.DBR };
		int[] tetrad1Order = { 0, 1, 2, 3 };
		final CubiePositions[] tetrad2 = { CubiePositions.DBL, CubiePositions.DFR, CubiePositions.UBR, CubiePositions.UFL };
		final int[] multipliers = { 1, 1, 2, 6 }; // 0!, 1!, 2!, 3!
		int t2Perm = coord & 0x3;
		//System.out.println("t2Perm: " + t2Perm);
		coord >>= 2;
		//System.out.println("coord: " + coord);
		boolean[] used = { false, false, false, false };
		for (int i = tetrad1.length-1; i >= 0; i--) {
			int numCubiesHigher = coord / multipliers[i];
			//System.out.println("numCubiesHigher: " + numCubiesHigher);
			coord %= multipliers[i];
			//System.out.println("coord: " + coord);
			int j;
			for (j=used.length-1; j >= 0; j--) {
				if ((numCubiesHigher == 0) && (used[j] == false)) {
					used[j] = true;
					break;
				}
				if (used[j] == false) {
					numCubiesHigher--;
				}
			}
			
			positions.put(tetrad1[i], tetrad1[j]);
			tetrad1Order[i] = j;
		}
		
		for (int i=1; i < tetrad1Order.length; i++) {
			tetrad1Order[i] ^= tetrad1Order[0];
		}
		tetrad1Order[0] = 0;
		
		for (int i=0; i < tetrad1Order.length; i++) {
			int t2Idx = tetrad1Order[i] ^ t2Perm;
			positions.put(tetrad2[i], tetrad2[t2Idx]);
		}
	}
	
	static int getLRSlicePermCoord(EnumMap<CubiePositions, CubiePositions> positions) {
		int coord = 0;
		final CubiePositions[] tetrad1 = { CubiePositions.UF, CubiePositions.UB, CubiePositions.DF, CubiePositions.DB };
		final int[] multipliers = { 1, 1, 2, 6 }; // 0!, 1!, 2!, 3!
		int numCubiesHigher = 0;
		for (int i=0; i < tetrad1.length; i++) {
			CubiePositions cubieInPos = positions.get(tetrad1[i]);
			numCubiesHigher = 0;
			// Count the number of cubies at lower positions with a higher ordinality
			for (int j=0; j < i; j++) {
				int ordinal1 = 0, ordinal2 = 0;
				switch (positions.get(tetrad1[j])) {
				case UF:
					ordinal1 = 0;
					break;
				case UB:
					ordinal1 = 1;
					break;
				case DF:
					ordinal1 = 2;
					break;
				case DB:
					ordinal1 = 3;
					break;
				default:
					break;
				}
				switch (cubieInPos) {
				case UF:
					ordinal2 = 0;
					break;
				case UB:
					ordinal2 = 1;
					break;
				case DF:
					ordinal2 = 2;
					break;
				case DB:
					ordinal2 = 3;
					break;
				default:
					break;
				}
				if (ordinal1 > ordinal2) {
					numCubiesHigher++;
				}
			}
			coord += multipliers[i] * numCubiesHigher;
		}
		return coord;
	}
	
	static int getUDSlicePermCoord(EnumMap<CubiePositions, CubiePositions> positions) {
		int coord = 0;
		final CubiePositions[] tetrad1 = { CubiePositions.FL, CubiePositions.FR, CubiePositions.BL, CubiePositions.BR };
		final int[] multipliers = { 1, 1, 2, 6 }; // 0!, 1!, 2!, 3!
		int numCubiesHigher = 0;
		for (int i=0; i < tetrad1.length; i++) {
			CubiePositions cubieInPos = positions.get(tetrad1[i]);
			numCubiesHigher = 0;
			// Count the number of cubies at lower positions with a higher ordinality
			for (int j=0; j < i; j++) {
				int ordinal1 = 0, ordinal2 = 0;
				switch (positions.get(tetrad1[j])) {
				case FL:
					ordinal1 = 0;
					break;
				case FR:
					ordinal1 = 1;
					break;
				case BL:
					ordinal1 = 2;
					break;
				case BR:
					ordinal1 = 3;
					break;
				default:
					break;
				}
				switch (cubieInPos) {
				case FL:
					ordinal2 = 0;
					break;
				case FR:
					ordinal2 = 1;
					break;
				case BL:
					ordinal2 = 2;
					break;
				case BR:
					ordinal2 = 3;
					break;
				default:
					break;
				}
				if (ordinal1 > ordinal2) {
					numCubiesHigher++;
				}
			}
			coord += multipliers[i] * numCubiesHigher;
		}
		return coord;
	}
	
	static int getFBSlicePermCoord(EnumMap<CubiePositions, CubiePositions> positions) {
		int coord = 0;
		final CubiePositions[] slice = { CubiePositions.UL, CubiePositions.UR, CubiePositions.DL, CubiePositions.DR };
		final int[] multipliers = { 1, 1, 2, 6 }; // 0!, 1!, 2!, 3!
		int numCubiesHigher = 0;
		for (int i=0; i < slice.length; i++) {
			CubiePositions cubieInPos = positions.get(slice[i]);
			numCubiesHigher = 0;
			// Count the number of cubies at lower positions with a higher ordinality
			for (int j=0; j < i; j++) {
				int ordinal1 = 0, ordinal2 = 0;
				switch (positions.get(slice[j])) {
				case UL:
					ordinal1 = 0;
					break;
				case UR:
					ordinal1 = 1;
					break;
				case DL:
					ordinal1 = 2;
					break;
				case DR:
					ordinal1 = 3;
					break;
				default:
					break;
				}
				switch (cubieInPos) {
				case UL:
					ordinal2 = 0;
					break;
				case UR:
					ordinal2 = 1;
					break;
				case DL:
					ordinal2 = 2;
					break;
				case DR:
					ordinal2 = 3;
					break;
				default:
					break;
				}
				if (ordinal1 > ordinal2) {
					numCubiesHigher++;
				}
			}
			coord += multipliers[i] * numCubiesHigher;
		}
		return coord;
	}
	
	static void setFBSlicePermCoord(int coord, EnumMap<CubiePositions, CubiePositions> positions) {
		final CubiePositions[] tetrad1 = { CubiePositions.UL, CubiePositions.UR, CubiePositions.DL, CubiePositions.DR };
		final int[] multipliers = { 1, 1, 2, 6 }; // 0!, 1!, 2!, 3!
		boolean[] used = { false, false, false, false };
		for (int i = tetrad1.length-1; i >= 0; i--) {
			int numCubiesHigher = coord / multipliers[i];
			coord %= multipliers[i];
			int j;
			for (j=used.length-1; j >= 0; j--) {
				if ((numCubiesHigher == 0) && (used[j] == false)) {
					used[j] = true;
					break;
				}
				if ((used[j] == false)) {
					numCubiesHigher--;
				}
			}
			positions.put(tetrad1[i], tetrad1[j]);
		}
	}
	
	static void setUDSlicePermCoord(int coord, EnumMap<CubiePositions, CubiePositions> positions) {
		final CubiePositions[] tetrad1 = { CubiePositions.FL, CubiePositions.FR, CubiePositions.BL, CubiePositions.BR };
		final int[] multipliers = { 1, 1, 2, 6 }; // 0!, 1!, 2!, 3!
		boolean[] used = { false, false, false, false };
		for (int i = tetrad1.length-1; i >= 0; i--) {
			int numCubiesHigher = coord / multipliers[i];
			coord %= multipliers[i];
			int j;
			for (j=used.length-1; j >= 0; j--) {
				if ((numCubiesHigher == 0) && (used[j] == false)) {
					used[j] = true;
					break;
				}
				if (used[j] == false) {
					numCubiesHigher--;
				}
			}
			positions.put(tetrad1[i], tetrad1[j]);
		}
	}
	
	static void setLRSlicePermCoord(int coord, EnumMap<CubiePositions, CubiePositions> positions) {
		final CubiePositions[] tetrad1 = { CubiePositions.UF, CubiePositions.UB, CubiePositions.DF, CubiePositions.DB };
		final int[] multipliers = { 1, 1, 2, 6 }; // 0!, 1!, 2!, 3!
		boolean[] used = { false, false, false, false };
		for (int i = tetrad1.length-1; i >= 0; i--) {
			int numCubiesHigher = coord / multipliers[i];
			coord %= multipliers[i];
			int j;
			for (j=used.length-1; j >= 0; j--) {
				if ((numCubiesHigher == 0) && (used[j] == false)) {
					used[j] = true;
					break;
				}
				if (used[j] == false) {
					numCubiesHigher--;
				}
			}
			positions.put(tetrad1[i], tetrad1[j]);
		}
	}
	
	// returns a permutation number uniquely describing the orientation
	// of the edges
	static int getEdgeOriCoord(EnumMap<CubiePositions, Integer> orientations) {
		int coord = 0;
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.EDGE) {
				coord *= 2;
				if (orientations.get(pos) == ORIENTATION_FLIPPED) {
					coord += 1;
				}
			}
		}
		return coord;
	}
	
	static int getCornerOriCoord(EnumMap<CubiePositions, Integer> orientations) {
		int coord = 0;
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.CORNER) {
				coord *= 3;
				if (orientations.get(pos) == ORIENTATION_C) {
					coord += 1;
				}
				else if (orientations.get(pos) == ORIENTATION_CC) {
					coord += 2;
				}
			}
		}
		return coord;
	}
	
	// Only gets orientation of 7 corners
	static int getCornerOriCoord7(EnumMap<CubiePositions, Integer> orientations) {
		int coord = 0;
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.CORNER) {
				coord *= 3;
				if (orientations.get(pos) == ORIENTATION_C) {
					coord += 1;
				}
				else if (orientations.get(pos) == ORIENTATION_CC) {
					coord += 2;
				}
			}
		}
		return coord/3;
	}
	
	static void setCornerOriCoord7(int coord,
			EnumMap<CubiePositions, Integer> orientations) {
		CubiePositions[] pos = CubiePositions.values();
		int totalRot = 0;
		int coordCp = coord;
		for (int i=0; i < 7; i++) {
			totalRot += coordCp % 3;
			coordCp /= 3;
		}
		int lastCubieRot = (3 - (totalRot % 3)) % 3;
		coord *= 3;
		coord += lastCubieRot;
		for (int i=pos.length-1; i >= 0; i--) {
			if (pos[i].getType() == CubiePositions.CubieTypes.CORNER) {
				if ((coord % 3) == 1) {
					orientations.put(pos[i], ORIENTATION_C);
				}
				else if ((coord % 3) == 2) {
					orientations.put(pos[i], ORIENTATION_CC);
				}
				else {
					orientations.put(pos[i], ORIENTATION_GOOD);
				}
				coord /= 3;
			}
		}
	}
	
	// computes n choose k = n! / k! (n-k)!
	static int choose(int n, int k) {
		if (k > n) {
			return 0;
		}
		if (k == 0) {
			return 1;
		}
		return (n * choose(n-1, k-1)) / k;
	}
	
	// 12 choose 4 = 495. Therefore uses 9 bits
	static int getLREdgeSliceCoord(EnumMap<CubiePositions, CubiePositions> positions) {
		int coord = 0;
		int[] orderedPositions = new int[4];
		int numAdded = 0;
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.EDGE) {
				CubiePositions cubieInPos = positions.get(pos);
				switch (cubieInPos) {
				case UF:
				case UB:
				case DF:
				case DB:
					break;
				default:
					continue;
				}
				// subtract 8 b/c of the 8 corners before the edges
				// TODO define ordinal in this file/function
				orderedPositions[numAdded++] = pos.ordinal() - 8;
				//System.out.println("get: " + pos);
				// insert in sorted order
				for (int i=1; i < numAdded; i++) {
					if (orderedPositions[i] < orderedPositions[i-1]) {
						int temp = orderedPositions[i];
						orderedPositions[i] = orderedPositions[i-1];
						orderedPositions[i-1] = temp;
					}
				}
			}
		}
		for (int i=0; i < 4; i++) {
			coord += choose(orderedPositions[i], i+1);
		}
		return coord;
	}
	
	// TODO set edges to a valid config
	static void setLREdgeSliceCoord(int coord,
			EnumMap<CubiePositions, CubiePositions> positions) {
		/*
		CubiePositions[] pos = CubiePositions.values();
		for (int i=pos.length-1; i >= 0; i--) {
			if (pos[i].getType() == CubiePositions.CubieTypes.EDGE) {
				if ((coord & 1) == 1) {
					positions.put(pos[i], CubiePositions.UF);
				}
				else {
					positions.put(pos[i], CubiePositions.FL);
				}
				coord /= 2;
			}
		}
		*/
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.EDGE) {
				positions.put(pos, CubiePositions.FL);
			}
		}
		// the positions of the four LR slice edges
		CubiePositions[] filledPositions = new CubiePositions[4];
		int idx = 0;
		int k = 4;
		for (int i = 0; i < 4; i++) {
			int ordinal = 0;
			for (int j = 11; j >= 0; j--) {
				if (choose(j, k) <= coord) {
					ordinal = j;
					break;
				}
			}
			filledPositions[idx++] = CubiePositions.values()[ordinal+8];
			//System.out.println("set: " + filledPositions[idx-1]);
			coord -= choose(ordinal, k);
			k--;
		}
		for (CubiePositions pos : filledPositions) {
			positions.put(pos, CubiePositions.UF);
		}
	}
	
	// 12 choose 4 = 495. Therefore uses 9 bits
	static int getUDEdgeSliceCoord(EnumMap<CubiePositions, CubiePositions> positions) {
		int coord = 0;
		int[] orderedPositions = new int[4];
		int numAdded = 0;
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.EDGE) {
				CubiePositions cubieInPos = positions.get(pos);
				switch (cubieInPos) {
				case FL:
				case FR:
				case BL:
				case BR:
					break;
				default:
					continue;
				}
				// subtract 8 b/c of the 8 corners before the edges
				// TODO define ordinal in this file/function
				orderedPositions[numAdded++] = pos.ordinal() - 8;
				//System.out.println("get: " + pos);
				// insert in sorted order
				for (int i=1; i < numAdded; i++) {
					if (orderedPositions[i] < orderedPositions[i-1]) {
						int temp = orderedPositions[i];
						orderedPositions[i] = orderedPositions[i-1];
						orderedPositions[i-1] = temp;
					}
				}
			}
		}
		for (int i=0; i < 4; i++) {
			coord += choose(orderedPositions[i], i+1);
		}
		return coord;
	}
	
	// TODO set edges to a valid config
	static void setUDEdgeSliceCoord(int coord,
			EnumMap<CubiePositions, CubiePositions> positions) {
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.EDGE) {
				positions.put(pos, CubiePositions.UF);
			}
		}
		// the positions of the four LR slice edges
		CubiePositions[] filledPositions = new CubiePositions[4];
		int idx = 0;
		int k = 4;
		for (int i = 0; i < 4; i++) {
			int ordinal = 0;
			for (int j = 11; j >= 0; j--) {
				if (choose(j, k) <= coord) {
					ordinal = j;
					break;
				}
			}
			filledPositions[idx++] = CubiePositions.values()[ordinal+8];
			//System.out.println("set: " + filledPositions[idx-1]);
			coord -= choose(ordinal, k);
			k--;
		}
		for (CubiePositions pos : filledPositions) {
			positions.put(pos, CubiePositions.FL);
		}
	}
	
	static void setTetradCoord(int coord, EnumMap<CubiePositions, CubiePositions> positions) {
		final CubiePositions[] t1Perm =
			{ CubiePositions.UFR, CubiePositions.UBL, CubiePositions.DFL, CubiePositions.DBR };
		final CubiePositions[][] t2Perms = {
			{ CubiePositions.DBL,  CubiePositions.DFR, CubiePositions.UBR, CubiePositions.UFL },
			{ CubiePositions.DBL,  CubiePositions.DFR, CubiePositions.UFL, CubiePositions.UBR },
			{ CubiePositions.DBL,  CubiePositions.UBR, CubiePositions.DFR, CubiePositions.UFL }, // 0 2 1 3
			{ CubiePositions.DBL,  CubiePositions.UFL, CubiePositions.DFR, CubiePositions.UBR }, // 0 2 3 1
			{ CubiePositions.DBL,  CubiePositions.UBR, CubiePositions.UFL, CubiePositions.DFR },
			{ CubiePositions.DBL,  CubiePositions.UFL, CubiePositions.UBR, CubiePositions.DFR }
		};
		int t1PermIdx = 0;
		int t2PermIdx = 0;
		int tetrad2Perm = coord % 6;
		coord /= 6;
		int tetrad2Pos = coord;
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.CORNER) {
				if ((tetrad2Pos & 1) == 1) {
					positions.put(pos, t2Perms[tetrad2Perm][t2PermIdx++]);
				}
				else {
					positions.put(pos, t1Perm[t1PermIdx++]);
				}
				tetrad2Pos /= 2;
			}
		}
	}
	
	/* gets the permutation number of tetrad 2, which is defined to be (in order):
	 * DBL, DFR, UBR, UFL. the first tetrad is the diametrically opposite corners
	 * in the same order. also returns the positional coordinate of tetrad 2
	 */
	static int getTetradCoord(EnumMap<CubiePositions, CubiePositions> positions) {
		int permCoord = 0;
		int posCoord = 0;
		int coord = 0;
		final int DBLidx = 0;
		final int DFRidx = 1;
		final int UBRidx = 2;
		final int UFLidx = 3;
		
		// maps a position in tetrad 1 to the cubie in that position
		int[] t1PosToCubie = new int[4];
		
		// maps a cubie in tetrad 2 to its position
		int[] t2CubieToPos = new int[4];
		int permIdx = 0;
		
		// t2RelPerm maps each cubie in tetrad 1 to its position in tetrad 2
		// example: t1PosToCubie = { 0 3 2 1 }, t2CubieToPos = { 0 2 3 1 }
		//  t2PosToCubie would be: 0 3 1 2
		//  t2RelPerm = { 0 1 3 2 }
		//  t2RelPerm[2] = 3 says that the cubie in tetrad 2 that has the
		//  same # as the cubie as the third (index 2) cubie in tetrad 1 is
		//  the third cubie in tetrad 2
		int[] t2RelPerm = new int[4];
		
		int orderNum = 0;
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.CORNER) { 
				CubiePositions cubieInPos = positions.get(pos);
				switch (cubieInPos) {
				case UFL:
					t2CubieToPos[UFLidx] = permIdx++;
					break;
				case DBR:
					t1PosToCubie[orderNum++] = UFLidx;
					break;
				case DFR:
					t2CubieToPos[DFRidx] = permIdx++;
					break;
				case UBL:
					t1PosToCubie[orderNum++] = DFRidx;
					break;
				case UBR:
					t2CubieToPos[UBRidx] = permIdx++;
					break;
				case DFL:
					t1PosToCubie[orderNum++] = UBRidx;
					break;
				case DBL:
					t2CubieToPos[DBLidx] = permIdx++;
					break;
				case UFR:
					t1PosToCubie[orderNum++] = DBLidx;
					break;
				default:
					break;
				}
			}
		}
		
		for (int i=0; i < 4; i++) {
			t2RelPerm[i] = t2CubieToPos[t1PosToCubie[i]];
		}
		
		/* this for loop finds the equivalent permutation that
		 * begins with zero. there are 4! = 24 total permutations,
		 * but there are only 6 different permutations, as some
		 * different permutations are the same but represent
		 * different orientations of the cube. for example, the
		 * permutation 0 1 2 3 in two dimensions would look like:
		 * 0 1
		 * 2 3. flipping the cube upside down would make it look like:
		 * 2 3
		 * 0 1. therefore the permutations 0 1 2 3 and 2 3 0 1 are
		 * equivalent. the other two permutations that are equivalent
		 * are 3 2 1 0 and 1 0 3 2. the for loop is always able to
		 * find the equivalent permutation that begins with zero because
		 * a number XOR'ed with itself is zero, and also because the
		 * equivalent permutations can be found from one permutation
		 * by XOR'ing the permutation with 1, 2, and 3, and finally
		 * because XOR is its own inverse. therefore starting with
		 * the permutation that begins with zero and XOR'ing it with
		 * i = 1, 2, or 3 will place i in the first position, and
		 * XOR'ing that permutation with i again will undo it.
		 * example: start with the permutation 0 1 2 3
		 * XOR with 1: 1 0 3 2
		 * XOR with 2: 2 3 0 1
		 * XOR with 3: 3 2 1 0
		 * these are the same permutations as those found via cube
		 * rotations above
		 */
		for (int i=1; i < 4; i++) {
			t2RelPerm[i] ^= t2RelPerm[0];
		}
		// t2RelPerm[0] is zero, so the permutation number can
		//  be based on indices 1, 2, and 3. create a permutation
		//  number between 0 and 5 (inclusive)
		permCoord = t2RelPerm[1]*2-2;
		if (t2RelPerm[2] > t2RelPerm[3]) {
			permCoord++;
		}
		
		// TODO use combinatorial number system to reduce bits of
		// posCoord from 8 to 7
		CubiePositions[] pos = CubiePositions.values();
		for (int i=pos.length-1; i >= 0; i--) {
			if (pos[i].getType() == CubiePositions.CubieTypes.CORNER) {
				posCoord *= 2;
				CubiePositions cubieInPos = positions.get(pos[i]);
				switch (cubieInPos) {
				// tetrad 2 corners
				case UFL:
				case UBR:
				case DFR:
				case DBL:
					posCoord += 1;
				default:
					break;
				}
			}
		}
		coord = (posCoord * 6) + permCoord;
		
		return coord;
	}
	
	static void setEdgeOriCoord(int coord,
			EnumMap<CubiePositions, Integer> orientations) {
		int mask = 1 << CubiePositions.getNumEdges();
		for (CubiePositions pos : CubiePositions.values()) {
			if (pos.getType() == CubiePositions.CubieTypes.EDGE) {
				mask >>= 1;
				if ((coord & mask) != 0) {
					orientations.put(pos, ORIENTATION_FLIPPED);
				}
				else {
					orientations.put(pos, ORIENTATION_GOOD);
				}
			}
		}
	}
	
	static void setCornerOriCoord(int coord,
			EnumMap<CubiePositions, Integer> orientations) {
		CubiePositions[] pos = CubiePositions.values();
		for (int i=pos.length-1; i >= 0; i--) {
			if (pos[i].getType() == CubiePositions.CubieTypes.CORNER) {
				if ((coord % 3) == 1) {
					orientations.put(pos[i], ORIENTATION_C);
				}
				else if ((coord % 3) == 2) {
					orientations.put(pos[i], ORIENTATION_CC);
				}
				else {
					orientations.put(pos[i], ORIENTATION_GOOD);
				}
				coord /= 3;
			}
		}
	}
	
	static private int getNewOrientation(CubeFaces face, CubiePositions pos,
			EnumMap<CubiePositions, Integer> orientations) {
		int oldOri = orientations.get(pos);
		int oriChange = getOriChange(face, pos);
		int newOri = oldOri + oriChange;
		if (pos.getType() == CubiePositions.CubieTypes.CORNER) {
			newOri %= 3;
		}
		else {
			newOri %= 2;
		}
		return newOri;
	}
	
	static void turnClockwise(CubeFaces face,
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		CubiePositions[][] perms = CubiePositions.getPerm(face);
		for (CubiePositions[] perm : perms) {
			int oriIntoPos = orientations.get(perm[perm.length-1]);
			CubiePositions cubieIntoPos = positions.get(perm[perm.length-1]);
			for (CubiePositions pos : perm) {
				// cycle the orientation into its new position and save
				//  the orientation currently in that position
				oriIntoPos = orientations.put(pos, oriIntoPos);
				// get new orientation
				int newOriAtPos = getNewOrientation(face, pos, orientations);
				// update orientation
				orientations.put(pos, newOriAtPos);
				// update position
				cubieIntoPos = positions.put(pos, cubieIntoPos);
			}
		}
	}
	
	// returns a map from positions to orientations with all
	//  orientations good
	static EnumMap<CubiePositions, Integer> getSolvedOrientations() {
		EnumMap<CubiePositions, Integer> oris =
				new EnumMap<CubiePositions, Integer>(CubiePositions.class);
		for (CubiePositions pos : CubiePositions.values()) {
			oris.put(pos, CoordinateHelper.ORIENTATION_GOOD);
		}
		return oris;
	}
	
	static EnumMap<CubiePositions, CubiePositions> getSolvedPositions() {
		EnumMap<CubiePositions, CubiePositions> positions =
				new EnumMap<CubiePositions, CubiePositions>(CubiePositions.class);
		for (CubiePositions pos : CubiePositions.values()) {
			positions.put(pos, pos);
		}
		return positions;
	}
}
