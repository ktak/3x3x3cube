package thistlethwaite;

import java.util.EnumMap;

import rubiksCube.CubiePositions;
import rubiksCube.Moves;

// interface that implements functions specific to
//  a phase of the algorithm
public interface Phase {
	byte[] getPruningTable();
	Moves[] getAllowedMoves();
	int getCoord(
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions);
	void setCoord(
			int coord,
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions);
	int getGoalCoord();
	int getPhaseNum();
}
