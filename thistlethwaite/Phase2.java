package thistlethwaite;

import java.util.EnumMap;

import rubiksCube.CubiePositions;
import rubiksCube.Moves;

public class Phase2 implements Phase {
	
	private static final Moves[] allowedMoves = {
		Moves.U2,
		Moves.D2,
		Moves.F, Moves.F2, Moves.F3,
		Moves.B, Moves.B2, Moves.B3,
		Moves.L, Moves.L2, Moves.L3,
		Moves.R, Moves.R2, Moves.R3
	};
	
	private static final byte[] table = createPhase2PruningTable();
	// 3 possible orientations for each of 8 corners
	private static final int numCornerOrientations = 3*3*3*3*3*3*3;
	// uninitialized value for table
	static final byte UNDEF = -1;
	static final int goalCoord = (CoordinateHelper.getCornerOriCoord7(CoordinateHelper.getSolvedOrientations()) << 9) +
			CoordinateHelper.getLREdgeSliceCoord(CoordinateHelper.getSolvedPositions());
	
	private static byte[] createPhase2PruningTable() {
		EnumMap<CubiePositions, Integer> orientations =
				CoordinateHelper.getSolvedOrientations();
		EnumMap<CubiePositions, CubiePositions> positions =
				CoordinateHelper.getSolvedPositions();
		byte[] table = new byte[(1<<9) *
		                        numCornerOrientations];
		for (int i=0; i < (1<<9) * numCornerOrientations; i++) {
			table[i] = UNDEF;
		}
		int fromCoord = coord(orientations, positions);
		byte depth = 0;
		int numAtDepth = 0;
		// the goal state is distance zero from the goal state
		table[fromCoord] = depth;
		boolean workDone = true;
		while (workDone) {
			workDone = false;
			for (int i=0; i < (1<<9) * numCornerOrientations; i++) {
				if (table[i] != depth) {
					continue;
				}
				fromCoord = i;
				setcoord(fromCoord, orientations, positions);
				/*if (fromCoord != coord(orientations, positions)) {
					System.out.println("fromCoord: " + fromCoord);
					System.out.println("actual: " + coord(orientations, positions));
					throw new IllegalArgumentException();
				}*/
				//System.out.println("coord: " + fromCoord);
				for (Moves move : allowedMoves) {
					for (int j=0; j < move.getNumTurns(); j++) {
						CoordinateHelper.
							turnClockwise(move.getFace(), orientations, positions);
					}
					int toCoord = coord(orientations, positions);
					if (table[toCoord] == UNDEF) {
						table[toCoord] = (byte)(depth+1);
						numAtDepth++;
						workDone = true;
					}
					// restore the orientations by rotating a total of 4 times
					for (int j=move.getNumTurns(); j < 4; j++) {
						CoordinateHelper.
							turnClockwise(move.getFace(), orientations, positions);
					}
				}
			}
			System.out.println(numAtDepth + " at depth " + (depth+1));
			depth++;
			numAtDepth = 0;
			
		}
		
		return table;
	}
	
	private static void setcoord(int coord,
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		CoordinateHelper.setLREdgeSliceCoord(coord&0x1ff, positions);
		CoordinateHelper.setCornerOriCoord7(coord>>9, orientations);
	}
	
	private static int coord(EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		return (CoordinateHelper.getCornerOriCoord7(orientations) << 9) +
				CoordinateHelper.getLREdgeSliceCoord(positions);
	}
	
	@Override
	public byte[] getPruningTable() {
		return table;
	}

	@Override
	public Moves[] getAllowedMoves() {
		return allowedMoves;
	}

	@Override
	public int getCoord(EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		return coord(orientations, positions);
	}

	@Override
	public int getGoalCoord() {
		return goalCoord;
	}

	@Override
	public int getPhaseNum() {
		return 2;
	}

	@Override
	public void setCoord(int coord,
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		// TODO Auto-generated method stub
		
	}

}
