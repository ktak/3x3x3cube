package thistlethwaite;

import java.util.EnumMap;
import java.util.LinkedList;

import rubiksCube.CubiePositions;
import rubiksCube.Moves;


public class PhaseTableGenerator {
	
	private static final byte UNDEF_VALUE = -1;
	// fills the given phase's pruning/move table that maps
	//  the phase's rubik's cube coordinates to a depth from
	//  the goal coordinate
	static public void fillTable(Phase phase) {
		EnumMap<CubiePositions, Integer> orientations =
				CoordinateHelper.getSolvedOrientations();
		EnumMap<CubiePositions, CubiePositions> positions =
				CoordinateHelper.getSolvedPositions();
		byte[] table = phase.getPruningTable();
		for (int i=0; i < table.length; i++) {
			table[i] = UNDEF_VALUE;
		}
		int fromCoord = phase.getCoord(orientations, positions);
		byte depth = 0;
		int numAtDepth = 0;
		// the goal state is distance zero from the goal state
		table[fromCoord] = depth;
		LinkedList<Integer> queue = new LinkedList<Integer>();
		queue.add(fromCoord);
		while (!queue.isEmpty()) {
			fromCoord = queue.remove();
			phase.setCoord(fromCoord, orientations, positions);
			if (table[fromCoord] != depth) {
				System.out.println(numAtDepth + " at depth " + (depth+1));
				numAtDepth = 0;
				depth = table[fromCoord];
			}
			for (Moves move : phase.getAllowedMoves()) {
				for (int i=0; i < move.getNumTurns(); i++) {
					CoordinateHelper.
						turnClockwise(move.getFace(), orientations, positions);
				}
				int toCoord = phase.getCoord(orientations, positions);
				if (table[toCoord] == UNDEF_VALUE) {
					table[toCoord] = (byte)(depth+1);
					queue.add(toCoord);
					numAtDepth++;
				}
				// restore the orientations by rotating a total of 4 times
				for (int i=move.getNumTurns(); i < 4; i++) {
					CoordinateHelper.
						turnClockwise(move.getFace(), orientations, positions);
				}
			}
		}
	}
}
