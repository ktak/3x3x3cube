package thistlethwaite;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

import rubiksCube.CubeFaces;
import rubiksCube.CubiePositions;
import rubiksCube.Moves;

public class ThistlethwaiteSolver {
	
	private boolean phase1Solved = false;
	private boolean phase2Solved = false;
	private boolean phase3Solved = false;
	private boolean phase4Solved = false;
	
	private EnumMap<CubiePositions, Integer> orientations =
			new EnumMap<CubiePositions, Integer>(CubiePositions.class);
	private EnumMap<CubiePositions, CubiePositions> positions =
			new EnumMap<CubiePositions, CubiePositions>(CubiePositions.class);
	
	public ThistlethwaiteSolver() {
		for (CubiePositions pos : CubiePositions.values()) {
			orientations.put(pos, CoordinateHelper.ORIENTATION_GOOD);
			positions.put(pos,  pos);
		}
	}
	
	public List<Moves> solvePhase(Phase p) {
		if (p.getPhaseNum() <= 0) {
			throw new IllegalArgumentException();
		}
		
		if (p.getPhaseNum() > 4) {
			throw new IllegalArgumentException();
		}
		
		if ((p.getPhaseNum() > 1) && (phase1Solved == false)) {
			throw new IllegalArgumentException();
		}
		
		if ((p.getPhaseNum() > 2) && (phase2Solved == false)) {
			throw new IllegalArgumentException();
		}
		
		if ((p.getPhaseNum() > 3) && (phase3Solved == false)) {
			throw new IllegalArgumentException();
		}
		
		System.out.println("Solving phase");
		ArrayList<Moves> solution = new ArrayList<Moves>();
		boolean solved = false;
		byte[] table = p.getPruningTable();
		int solutionDepth = table[p.getCoord(orientations, positions)];
		if (solutionDepth == -1) {
			System.out.println(p.getCoord(orientations, positions));
			for (CubiePositions pos : CubiePositions.values()) {
				CubiePositions cubieInPos = positions.get(pos);
				int rot = orientations.get(pos);
				System.out.println(pos + ": " + cubieInPos + " " + rot);
			}
			throw new IllegalArgumentException();
		}
		while (!solved) {
			for (Moves move : p.getAllowedMoves()) {
				// perform the move by rotating the move's
				//  face the correct number of times
				for (int i=0; i < move.getNumTurns(); i++) {
					CoordinateHelper.
						turnClockwise(move.getFace(), orientations, positions);
				}
				//System.out.println("move: " + move + " " + table[p.getCoord(orientations, positions)]);
				if (table[p.getCoord(orientations, positions)] == solutionDepth-1) {
					solution.add(move);
					solutionDepth--;
					System.out.println("Solution depth: " + solutionDepth);
					if (solutionDepth == 0) {
						solved = true;
					}
					break;
				}
				// restore the cube by rotating a total of 4 times
				for (int i=move.getNumTurns(); i < 4; i++) {
					CoordinateHelper.turnClockwise(move.getFace(), orientations, positions);
				}
				//System.out.println(p.getCoord(orientations, positions));
			}
		}
		
		switch (p.getPhaseNum()) {
		case 1:
			phase1Solved = true;
			break;
		case 2:
			phase2Solved = true;
			break;
		case 3:
			phase3Solved = true;
			break;
		case 4:
			phase4Solved = true;
			break;
		default:
			break;
		}
		return solution;
	}
	
	public static void main(String[] args) {
		ThistlethwaiteSolver ts = new ThistlethwaiteSolver();
		CoordinateHelper.turnClockwise(CubeFaces.D, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.D, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.D, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.L, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.R, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.U, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.U, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.U, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.F, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.F, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.F, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.B, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.B, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.B, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.D, ts.orientations, ts.positions);
		CoordinateHelper.turnClockwise(CubeFaces.D, ts.orientations, ts.positions);
		//CoordinateHelper.turnClockwise(CubeFaces.D, ts.orientations, ts.positions);
		
		List<Moves> sol = ts.solvePhase(new Phase1());
		for (Moves move : sol) {
			System.out.println(move);
		}
		sol = ts.solvePhase(new Phase2());
		for (Moves move : sol) {
			System.out.println(move);
		}
		sol = ts.solvePhase(new Phase3());
		for (Moves move : sol) {
			System.out.println(move);
		}
		sol = ts.solvePhase(new Phase4());
		for (Moves move : sol) {
			System.out.println(move);
		}
		
		for (CubiePositions pos : CubiePositions.values()) {
			CubiePositions cubieInPos = ts.positions.get(pos);
			int rot = ts.orientations.get(pos);
			System.out.println(pos + ": " + cubieInPos + " " + rot);
		}
		
	}
}
