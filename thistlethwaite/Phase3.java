package thistlethwaite;

import java.util.EnumMap;
import java.util.LinkedList;

import rubiksCube.CubiePositions;
import rubiksCube.Moves;

public class Phase3 implements Phase {

	private static final Moves[] allowedMoves = {
		Moves.U2,
		Moves.D2,
		Moves.F2,
		Moves.B2,
		Moves.L, Moves.L2, Moves.L3,
		Moves.R, Moves.R2, Moves.R3
	};
	
	private static final byte[] table = createPhase3PruningTable();
	// uninitialized value for table
	static final byte UNDEF = -1;
	static final int goalCoord = coord(CoordinateHelper.getSolvedOrientations(), CoordinateHelper.getSolvedPositions());
	
	private static byte[] createPhase3PruningTable() {
		EnumMap<CubiePositions, Integer> orientations =
				CoordinateHelper.getSolvedOrientations();
		EnumMap<CubiePositions, CubiePositions> positions =
				CoordinateHelper.getSolvedPositions();
		byte[] table = new byte[1<<20];
		for (int i=0; i < 1<<20; i++) {
			table[i] = UNDEF;
		}
		int fromCoord = coord(orientations, positions);
		byte depth = 0;
		int numAtDepth = 0;
		// the goal state is distance zero from the goal state
		table[fromCoord] = depth;
		LinkedList<Integer> queue = new LinkedList<Integer>();
		queue.add(fromCoord);
		while (!queue.isEmpty()) {
			fromCoord = queue.remove();
			setcoord(fromCoord, orientations, positions);
			if (table[fromCoord] != depth) {
				System.out.println(numAtDepth + " at depth " + (depth+1));
				numAtDepth = 0;
				depth = table[fromCoord];
			}
			//System.out.println("coord: " + fromCoord);
			for (Moves move : allowedMoves) {
				for (int i=0; i < move.getNumTurns(); i++) {
					CoordinateHelper.
						turnClockwise(move.getFace(), orientations, positions);
				}
				int toCoord = coord(orientations, positions);
				if (table[toCoord] == UNDEF) {
					table[toCoord] = (byte)(depth+1);
					queue.add(toCoord);
					numAtDepth++;
				}
				// restore the orientations by rotating a total of 4 times
				for (int i=move.getNumTurns(); i < 4; i++) {
					CoordinateHelper.
						turnClockwise(move.getFace(), orientations, positions);
				}
			}
		}
		
		return table;
	}
	
	private static void setcoord(int coord,
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		CoordinateHelper.setUDEdgeSliceCoord(coord&0x1ff, positions);
		CoordinateHelper.setTetradCoord(coord>>9, positions);
	}
	
	private static int coord(EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		return (CoordinateHelper.getTetradCoord(positions) << 9) + CoordinateHelper.getUDEdgeSliceCoord(positions);
	}
	
	@Override
	public byte[] getPruningTable() {
		return table;
	}

	@Override
	public Moves[] getAllowedMoves() {
		return allowedMoves;
	}

	@Override
	public int getCoord(EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		return coord(orientations, positions);
	}

	@Override
	public int getGoalCoord() {
		return goalCoord;
	}

	@Override
	public int getPhaseNum() {
		return 3;
	}

	@Override
	public void setCoord(int coord,
			EnumMap<CubiePositions, Integer> orientations,
			EnumMap<CubiePositions, CubiePositions> positions) {
		// TODO Auto-generated method stub
		
	}
}
